local null_ls_status_ok, null_ls = pcall(require, "null-ls")
if not null_ls_status_ok then
  return
end

-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/formatting
local formatting = null_ls.builtins.formatting
-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/code_actions
local code_actions = null_ls.builtins.code_actions
local diagnostics = null_ls.builtins.diagnostics

-- https://github.com/prettier-solidity/prettier-plugin-solidity
function SetupNullLs(formatting_sources, coda_actions_sources, diagnostics_sources)
  local f = {}
  for source_name,with in pairs(formatting_sources) do
    local source = formatting[source_name]
    if with then
      source = source.with(with)
    end
    table.insert(f, source)
  end
  local ca = {}
  for source_name,with in pairs(coda_actions_sources) do
    local source = code_actions[source_name]
    if with then
      source = source.with(with)
    end
    table.insert(ca, source)
  end
  local d = {}
  for source_name,with in pairs(diagnostics_sources) do
    local source = diagnostics[source_name]
    if with then
      source = source.with(with)
    end
    table.insert(d, source)
  end
  local sources = {}
  for _,source in ipairs(f) do
    table.insert(sources, source)
  end
  for _,source in ipairs(ca) do
    table.insert(sources, source)
  end
  for _,source in ipairs(d) do
    table.insert(sources, source)
  end
  null_ls.setup {
    debug = true,
    debounce = 150,
    save_after_format = false,
    default_timeout = -1,
    sources = sources,
  }
end

