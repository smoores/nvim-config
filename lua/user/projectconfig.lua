local projectconfig_is_ok, project_config = pcall(require, "nvim-projectconfig")
if not projectconfig_is_ok then
  return
end

project_config.setup({ silent = false })
